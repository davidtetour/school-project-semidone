<?php
/** Created by David Tetour on Feb 12th 2018. */

require 'utils.php';

session_start();
$_SESSION = array();
setcookie(session_name(), '', time() - 2592000, '/');
session_destroy();
redirect('goodbye.html');
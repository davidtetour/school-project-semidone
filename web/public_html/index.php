<?php
/** Created by David Tetour on Feb 12th 2018. */
require_once 'connectivity.php';
require 'utils.php';

define("PAGE_SIZE", 20);

session_start();
$username = NULL;
if (isset($_SESSION['user-id']) && isset($_SESSION['username'])) {
    $user_id = $_SESSION['user-id'];
    $username = $_SESSION['username'];
} else {
    redirect('welcome.html');
}
if (!isset($_SESSION['refresh-token'])) $_SESSION['refresh-token'] = uniqid();
$refresh_token = $_SESSION['refresh-token'];

$conn = new mysqli($db_hostname, $db_username, $db_password, $db_database);
if ($conn->connect_error) fail($conn->connect_error);

// initializing paging related variables
$page = isset($_GET['page']) ? (int)extract_from_get($conn, 'page') : 1;

// deletes a task
if (isset($_POST['delete']) && isset($_POST['id'])) {
    $id = extract_from_post($conn, 'id');
    $page = find_page_number($conn, $user_id, $id, PAGE_SIZE);
    $query = "DELETE FROM tasks WHERE id='$id'";
    $result = $conn->query($query);
    if (!$result) fail("DELETE failed: $query");
}

// edits a task
if (isset($_POST['edit']) && isset($_POST['id']) && isset($_POST['state'])
    && isset($_POST['description']) && isset($_POST['due'])) {
    $id = extract_from_post($conn, 'id');
    $state = extract_from_post($conn, 'state');
    $description = extract_from_post($conn, 'description');
    $due = extract_from_post($conn, 'due');
    $due = !empty($due) ? "'$due'" : 'NULL';
    $query = "UPDATE tasks SET state='$state', description='$description', due=$due WHERE id=$id";
    $result = $conn->query($query);
    if (!$result) fail("UPDATE failed: $query");
    $page = find_page_number($conn, $user_id, $id, PAGE_SIZE);
}

// adds a new task
if (isset($_POST['description']) && isset($_POST['add']) && isset($_POST['refresh-token'])
    && extract_from_post($conn, 'refresh-token') == $refresh_token) {
	$refresh_token = uniqid();
    $_SESSION['refresh-token'] = $refresh_token;
    $description = extract_from_post($conn, 'description');
    if (isset($_POST['due'])) $due_date = extract_from_post($conn, 'due');
    $due = !empty($due_date) ? "'$due_date'" : 'NULL';
    $query = "INSERT INTO tasks(user_id, state, due, description) VALUES('$user_id','active', $due, '$description')";
    $result = $conn->query($query);
    if (!$result) fail("INSERT failed: $query");
    $page = 1;
}

$url = $_SERVER[REQUEST_URI];
if (strpos($url,'page') === false) {
    $new_url = $url . ((strpos($url, '?') === false) ? '?' : '&') . 'page=' . $page;
    header("Location: $new_url");
}

/**
 * Composes a form navigating user to a given page number.
 *
 * @param page - number
 *
 * @return string html form
 */
function navigation_form($page) {
    return <<<_END
<form action="index.php" method="get" class="page">
    <input type="submit" name="page" value="$page">
</form>
_END;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tasks - SemiDone</title>
	<script type="text/javascript" src="main.js"></script>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>

<div class="header">
	SemiDone
	<div class="right"><a href='logout.php'>log out</a></div>
	<?php if (!is_null($username)) echo "<div class='right'>hello $username</div>" ?>
</div>

<div class="main">
	<div class="addition">
	<form action="index.php" method="post">
		<input type="hidden" name="refresh-token" value="<?php echo $refresh_token ?>">
		<label>description <input type="text" name="description" autofocus required class="description entry"></label>
		<label>due <input type="date" name="due" id="datePicker" class="entry"></label>
		<input type="submit" name="add" value="ADD" id="add">
	</form>
	</div>

    <?php
    // creates a listing of tasks for display
    $skip = ($page - 1) * PAGE_SIZE;
    $query = "SELECT * FROM tasks WHERE user_id=$user_id ORDER BY id DESC LIMIT $skip, " . PAGE_SIZE;
    $result = $conn->query($query);
    if (!$result) fail ("Database access failed: " . $conn->error);
    for ($j = 0; $j < $result->num_rows; ++$j) {
        $result->data_seek($j);
        $row = $result->fetch_array(MYSQLI_NUM);
        $active_selected = ($row[2] == 'active') ? 'selected' : '';
        $done_selected = ($row[2] == 'done') ? 'selected' : '';
        $created = substr($row[5], 0, (strlen($row[5]) - 9));
        $description = htmlentities($row[4]);
        echo <<<_END
<div class="content">
<form action="index.php" method="post" class="inline">
    <input type="hidden" name="id" value="$row[0]" class="narrow">
    <select name="state" title="task state">
        <option value="active" $active_selected>active</option>
        <option value="done" $done_selected>done</option>
    </select>
    <input type="text" title="task description" name="description" value="$description" required class="description">
    <input type="date" title="due date" name="due" value="$row[3]" class="date">
    <input type="submit" title="save row" name="edit" value="SAVE" class="narrow">
    <input type="submit" title="delete row" name="delete" value="DELETE" class="narrow">
    <input type="text" title="date created" name="created" value="$created" readonly class="date">
</form>
</div>
_END;
    }
    $query = "SELECT COUNT(*) FROM tasks WHERE user_id=$user_id";
    $result = $conn->query($query);
    if (!$result) fail("user entry count SELECT failed: $query");
    $entry_count = $result->fetch_row()[0];
    $page_count = ceil($entry_count / PAGE_SIZE);
    // optional paging
    if ($entry_count > PAGE_SIZE) {
        $navigation = '<div class="navigation">';
        $navigation .= $page > 6 ? navigation_form(1) . '… ' : "";
        for ($n = max(1, $page - 5); $n < $page; $n++) {
            $navigation .= navigation_form($n);
        }
        $navigation .= "<span class='current'>$page</span>";
        for ($n = $page + 1; $n <= $page + 5 && $n <= $page_count; $n++) {
            $navigation .= navigation_form($n);
        }
        $navigation .= $page <= $page_count - 6 ? ' …' . navigation_form($page_count) : "";
        $navigation .= "</div>";

        echo $navigation;
    }

    $result->close();
    $conn->close();
    ?>
</div>

<div class="footer">
	ZWA class project, &copy;2018, David Tetour
</div>

</body>
</html>
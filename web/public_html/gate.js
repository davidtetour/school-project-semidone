/**
 * Main function that assigns event listeners.
 */
window.onload = function () {

    // essential state variables
    var usernameValidated = false;
    var passwordValidated = false;

    // accessed and manipulated elements
    var oldUsername = document.getElementById('username')
    var newUsername = document.getElementById('new-username');
    var usernameComment = document.getElementById('username-comment');
    var firstPassword = document.getElementById('first-password');
    var secondPassword = document.getElementById('second-password');
    var passwordComment = document.getElementById('password-comment');
    var signUpSubmission = document.getElementById('signup-submission');

    preFillUser();
    newUsername.addEventListener('keyup', validateUsername);
    secondPassword.addEventListener('keyup', comparePasswords);

    /**
     * Given username is checked for availability and symbol composition.
     * If username is taken or not pass symbol safety check it disables signup submission.
     * Communicates asynchronously with 'validation.php'.
     */
    function validateUsername() {
        var bannedMessage = "username contains banned symbols";
        // short-circuits server-side validation
        if (!newUsername.value.match(/^[-\w\d _]+$/) ) {
            warn(bannedMessage);
            return;
        }
        var request = new XMLHttpRequest();
        request.open('POST', 'validation.php');
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.onload = function () {
            if (request.status === 200 && request.readyState === 4) {
                if (request.responseText === 'available') confirm('username is available');
                else if (request.responseText === 'taken') warn('username is taken');
                else if (request.responseText === 'invalid') warn(bannedMessage);
            } else console.log('Request failed. The returned status is ' + request.status);
            trySubmission()
        };
        if (newUsername.value === '') usernameComment.innerHTML = '';
        else request.send(encodeURI('name=' + newUsername.value));
    }

    /**
     * Changes sign-up elements to a warning styling.
     * Attaches the given text to username comment element.
     */
    function warn(text) {
        usernameValidated = false;
        usernameComment.className = 'comment warning';
        newUsername.className = 'warning';
        usernameComment.innerHTML = text;
    }

    /**
     * Changes sign-up elements to a confirmation styling.
     * Attaches the given text to username comment element.
     */
    function confirm(text) {
        usernameValidated = true;
        usernameComment.className = 'comment confirmation';
        newUsername.className = 'confirmation';
        usernameComment.innerHTML = text;
    }

    /**
     * Checks if both username and password have been validated and if so enables signup submission.
     * Otherwise, disables sign-up submission.
     */
    function trySubmission() {
        if (usernameValidated && passwordValidated) signUpSubmission.removeAttribute('disabled');
        else signUpSubmission.setAttribute('disabled', 'disabled');
    }

    /**
     * Compares passwords to check they have been retyped correctly.
     * Non-matching passwords disable sign-up submission.
     */
    function comparePasswords() {
        var match = firstPassword.value === secondPassword.value;
        if (firstPassword.value === '' && secondPassword.value === '') return false;
        if (match) {
            passwordComment.innerHTML = 'passwords match';
            passwordComment.className = 'comment confirmation';
            secondPassword.className = 'confirmation';
            firstPassword.className = 'confirmation';
        } else {
            passwordComment.innerHTML = 'passwords do not match';
            passwordComment.className = 'comment warning';
            secondPassword.className = 'warning';
            firstPassword.className = 'warning';
        }
        passwordValidated = match;
        trySubmission();
    }

    /**
     * Accesses input element with 'username' id and
     * sets its value attribute to the value of 'usr' query parameter.
     * Intended to pre-fill username in a login field.
     */
    function preFillUser() {
        var params = (new URL(document.location)).searchParams;
        var username = params.get("usr");
        if (username) oldUsername.setAttribute("value", username);
    }
};

<?php
/** Created by David Tetour on Feb 12th 2018. */
require_once 'connectivity.php';
require 'utils.php';

$conn = new mysqli($db_hostname, $db_username, $db_password, $db_database);
if ($conn->connect_error) fail($conn->connect_error);

// optionally performs validation of username, if username if provided in request
if (isset($_POST['name'])) {
    $name = extract_from_post($conn, 'name');
    if (!find_if_available($conn, $name)) echo 'taken';
    else if (!is_valid($name)) echo 'invalid';
    else echo 'available';
}

$conn->close();

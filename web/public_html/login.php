<?php
/** Created by David Tetour on Feb 12th 2018. */

require_once 'connectivity.php';
require 'utils.php';

$conn = new mysqli($db_hostname, $db_username, $db_password, $db_database);
if ($conn->connect_error) fail($conn->connect_error);

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = extract_from_post($conn, 'username');
    $raw_username = $_POST['username'];
    $password = extract_from_post($conn, 'password');
    $query  = "SELECT * FROM users WHERE username='$username'";
    $result = $conn->query($query);
    if (!$result) fail("SELECT failed: $query");
    $row = $result->fetch_assoc();
    if (!password_verify($password, $row['password'])) {
        $query_params = (strpos($raw_username, '&') !== false) ? "" : "?usr=$raw_username";
        redirect("retry.html" . $query_params);
    } else {
        session_start();
        $_SESSION['username'] = $raw_username;
        $_SESSION['user-id'] = $row['id'];
        redirect('index.php');
    }
}

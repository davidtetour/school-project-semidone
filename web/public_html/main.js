window.onload = preFillCurrentDate;

/**
 * Accesses input element with 'username' id and
 * sets its value attribute to the value of 'usr' query parameter.
 * Intended to pre-fill username in a login field.
 */
function preFillCurrentDate() {
	document.getElementById('datePicker').valueAsDate = new Date();
}

<?php
/** Created by David Tetour on Feb 12th 2018. */
require_once 'connectivity.php';
require 'utils.php';

$conn = new mysqli($db_hostname, $db_username, $db_password, $db_database);
if ($conn->connect_error) fail($conn->connect_error);

if (isset($_POST['username']) && isset($_POST['first-password']) && isset($_POST['second-password'])) {
    $username = extract_from_post($conn, 'username');
    $password1 = extract_from_post($conn, 'first-password');
    $password2 = extract_from_post($conn, 'second-password');

    // check that password was retyped correctly
    if ($password1 != $password2) {
        redirect('rejected-password.html');
    }

    // check that username is valid and available
    if (!find_if_available($conn, $username) || !is_valid($username)) {
        redirect('rejected-username.html');
    }

    // register user
    $hash = password_hash($password1, PASSWORD_DEFAULT);
    $query = "INSERT INTO users(username, password) VALUES ('$username', '$hash')";
    $result = $conn->query($query);
    if (!$result) fail("INSERT failed: $query");

    // login user
    $query  = "SELECT id FROM users WHERE username='$username'";
    $result = $conn->query($query);
    if (!$result) fail("SELECT failed: $query");
    $row = $result->fetch_assoc();
    session_start();
    $_SESSION['username'] = $username;
    $_SESSION['user-id'] = $row['id'];
    redirect('index.php');
    $result->close();
}

$conn->close();
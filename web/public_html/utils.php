<?php
/** Created by David Tetour on Feb 12th 2018. */

/**
 * Extracts safely the value of a given post method request parameter.
 *
 * @param conn - database connection
 * @param string - request parameter
 *
 * @return string value of request parameter
 */
function extract_from_post($conn, $string) {
    return $conn->real_escape_string($_POST[$string]);
}

/**
 * Extracts safely the value of a given get method request parameter.
 *
 * @param conn - database connection
 * @param string - request parameter
 *
 * @return string value of request parameter
 */
function extract_from_get($conn, $string) {
    return $conn->real_escape_string($_GET[$string]);
}

/**
 * Sanitizing removes tags an html entities from given string
 *
 * @param string - text to be sanitized
 *
 * @return string - sanitized form of the given string
 */
function sanitize($string){
    return htmlentities(strip_tags($string));
}

/**
 * Redirects via 302 response code to given url.
 *
 * @param url - redirect destination
 */
function redirect($url) {
    header('Location: ' . $url, true, 302);
    exit();
}

/**
 * Logs the error that occurred and redirects user to error page.
 *
 * @param $error - serializable error representation
 *
 * @return void
 */
function fail($error) {
    // todo log error to a log file
    redirect('error.html');
}

/**
 * Looks up given username in users table to determine if it is available.
 *
 * @param $connection - database connection
 * @param $username string
 *
 * @return boolean - true if name available, false if taken
 */
function find_if_available($connection, $username) {
    $query = "SELECT id FROM users WHERE username='$username' LIMIT 1";
    $result = $connection->query($query);
    if (!$result) fail("SELECT failed: $query");
    $available = !$result->num_rows > 0;
    $result->close();
    return $available;
}

/**
 * Looks up given task in tasks table to determine its page number based on given page size.
 *
 * @param $connection - database connection
 * @param $user_id int
 * @param $task_id int
 * @param $page_size int
 *
 * @return int page number of the given task
 */
function find_page_number($connection, $user_id, $task_id, $page_size) {
    $query = "SELECT COUNT(*) FROM tasks WHERE(id >= $task_id AND user_id = $user_id)";
    $result = $connection->query($query);
    if (!$result) fail("SELECT failed: $query");
    $ordinal = $result->fetch_row()[0];
    $result->close();
    $page_number = ceil($ordinal / $page_size);
    return $page_number;
}

/**
 * Determines whether the given name is composed of only accepted characters.
 * Accepted characters are alpha-numeric characters, whitespaces, dashes, and underscores.
 *
 * @param $name string that is tested
 *
 * @return boolean true if name is valid string, false if the name is not a string or is an invalid string
 */
function is_valid($name) {
    if (!is_string($name)) return false;
    return ($name == preg_replace('/[^-\p{L}\p{N} _]+/', '', $name));
}



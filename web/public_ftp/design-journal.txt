PHASE 1
Single user, single task table setup, unprotected version.
Primary goal is to have partial solution publicly accessible on university network.

database design:
database zwa:
	table tasks:
		columns names and data types:
		id			auto-incremented integer
		state		active or done
		due			date
		description	variable length character string
		created		timestamp

I wasn't successful accessing the university server, so I purchased inexpensive LAMP hosting and
routed one of my domains to it. After overcoming slight struggles with setting up ftp access
and creating a database I was capable of putting out complete phase 1 version.

PHASE 2
Full database structure with separate tables for users and tasks.
Primary goal is to provide support for multiple users.

database design:
database zwa:
	table users:
		column names and data types:
		id			auto-incremented integer
		username	variable length character string
		password*	variable length character string
		created		timestamp
		*contained text does not contain password, rather a salted hash
	table tasks:
		columns names and data types:
        id			auto-incremented integer
        state		active or done
        due			date
        description	variable length character string
        created		timestamp
        user_id		integer

It is possible now to log-in and see tasks belonging to a particular user. User specific content is enabled through
php sessions.

PHASE 3
Support complete user administration with logging out and registering new users.
There will be a convenient auto-focus on all login sections.
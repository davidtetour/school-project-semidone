INSERT INTO tasks(state, due, description, user_id) VALUES
	('done', '2018-02-09', 'cook couscous with chili', 1),
	('done', '2018-02-10', 'read chapter 4 of scala textbook', 1),
	('active', NULL, 'iron shirts', 1),
	('active', NULL, 'finish zwa project', 1),
	('active', '2018-02-11', 'sweep floor in kitchen', 1),
	('done', NULL, 'visit grandma', 2),
	('active', '2018-02-14', 'buy groceries', 2)
;
